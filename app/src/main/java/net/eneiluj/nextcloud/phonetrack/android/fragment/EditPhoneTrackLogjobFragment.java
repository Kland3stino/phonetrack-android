package net.eneiluj.nextcloud.phonetrack.android.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
//import android.preference.EditTextPreference;
import androidx.core.view.MenuItemCompat;
import androidx.preference.EditTextPreference;
//import android.preference.ListPreference;
//import android.preference.Preference;
import androidx.preference.Preference;
//import android.preference.PreferenceFragment;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.ShareActionProvider;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import net.eneiluj.nextcloud.phonetrack.R;
import net.eneiluj.nextcloud.phonetrack.android.activity.SettingsActivity;
import net.eneiluj.nextcloud.phonetrack.model.DBLogjob;
import net.eneiluj.nextcloud.phonetrack.model.DBSession;
import net.eneiluj.nextcloud.phonetrack.persistence.SessionServerSyncHelper;
import net.eneiluj.nextcloud.phonetrack.util.ICallback;

import static android.webkit.URLUtil.isValidUrl;

//public abstract class EditLogjobFragment extends Fragment implements CategoryDialogFragment.CategoryDialogListener {
//public class EditLogjobFragment extends PreferencesFragment {
public class EditPhoneTrackLogjobFragment extends EditLogjobFragment {

    private EditTextPreference editToken;
    private EditTextPreference editDevicename;

    private AlertDialog.Builder selectBuilder;
    private AlertDialog selectDialog;

    private AlertDialog.Builder fromUrlBuilder;
    private AlertDialog fromUrlDialog;
    private EditText fromUrlEdit;

    private List<DBSession> sessionList;
    private List<String> sessionNameList;
    private List<String> sessionIdList;

    @Override
    public void onCreatePreferencesFix(Bundle savedInstanceState, String rootkey) {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.activity_edit);

        endOnCreate();

        System.out.println("PHONEFRAG on create : "+logjob);

        Preference tokenPref = findPreference("token");
        tokenPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference,
                                              Object newValue) {
                EditTextPreference pref = (EditTextPreference) findPreference("token");
                String newValueString = (String) newValue;
                if (newValueString == null || newValueString.equals("")) {
                    showToast(getString(R.string.error_invalid_token), Toast.LENGTH_LONG);
                    return false;
                }
                else {
                    pref.setText((String) newValue);
                    pref.setSummary((CharSequence) newValue);
                    //saveLogjob(null);
                    return true;
                }
            }

        });
        Preference devicenamePref = findPreference("devicename");
        devicenamePref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference,
                                              Object newValue) {
                EditTextPreference pref = (EditTextPreference) findPreference("devicename");
                String newValueString = (String) newValue;
                if (newValueString == null || newValueString.equals("")) {
                    showToast(getString(R.string.error_invalid_devname), Toast.LENGTH_LONG);
                    return false;
                }
                else {
                    pref.setText((String) newValue);
                    pref.setSummary((CharSequence) newValue);
                    //saveLogjob(null);
                    return true;
                }
            }

        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (db.getSessionsNotShared().size() == 0) {
            MenuItem itemSelectSession = menu.findItem(R.id.menu_selectSession);
            itemSelectSession.setVisible(false);
        }
        menu.findItem(R.id.menu_share).setVisible(db.getPhonetrackServerSyncHelper().isConfigured(getActivity()));
        menu.findItem(R.id.menu_share).setTitle(R.string.menu_share_dev_title);
    }

    /**
     * Save the current state in the database and schedule synchronization if needed.
     *
     * @param callback Observer which is called after save/synchronization
     */
    @Override
    protected void saveLogjob(@Nullable ICallback callback) {
        Log.d(getClass().getSimpleName(), "saveData()");
        String newTitle = getTitle();
        String newUrl = getURL();
        String newToken = getToken();
        String newDevicename = getDevicename();
        int newMinTime = getMintime();
        int newMinDistance = getMindistance();
        int newMinAccuracy = getMinaccuracy();
        boolean newKeepGpsOn = getKeepGpsOn();

        // if this is an existing logjob
        if (logjob.getId() != 0) {
            if (logjob.getTitle().equals(newTitle) &&
                    logjob.getUrl().equals(newUrl) &&
                    logjob.getToken().equals(newToken) &&
                    logjob.getMinTime() == newMinTime &&
                    logjob.keepGpsOnBetweenFixes() == newKeepGpsOn &&
                    logjob.getMinDistance() == newMinDistance &&
                    logjob.getMinAccuracy() == newMinAccuracy &&
                    logjob.getDeviceName().equals(newDevicename)) {
                Log.v(getClass().getSimpleName(), "... not saving logjob, since nothing has changed");
            } else {
                System.out.println("====== update logjob");
                logjob = db.updateLogjobAndSync(logjob, newTitle, newToken, newUrl, newDevicename,
                        false, newMinTime, newMinDistance, newMinAccuracy, newKeepGpsOn, callback);
                notifyLoggerService(logjob.getId());
                //listener.onLogjobUpdated(logjob);
            }
        }
        // this is a new logjob
        else {
            DBLogjob newLogjob = new DBLogjob(0, newTitle, newUrl, newToken, newDevicename,
                    newMinTime, newMinDistance, newMinAccuracy, newKeepGpsOn ,
                    false, false, 0);
            long newId = db.addLogjob(newLogjob);
            notifyLoggerService(newId);
        }
    }

    public static EditPhoneTrackLogjobFragment newInstance(long logjobId) {
        EditPhoneTrackLogjobFragment f = new EditPhoneTrackLogjobFragment();
        Bundle b = new Bundle();
        b.putLong(PARAM_LOGJOB_ID, logjobId);
        f.setArguments(b);
        return f;
    }

    public static EditPhoneTrackLogjobFragment newInstanceWithNewLogjob(DBLogjob newLogjob) {
        EditPhoneTrackLogjobFragment f = new EditPhoneTrackLogjobFragment();
        Bundle b = new Bundle();
        b.putSerializable(PARAM_NEWLOGJOB, newLogjob);
        f.setArguments(b);
        return f;
    }

    private ICallback shareCallBack = new ICallback() {
        @Override
        public void onFinish() {
        }

        public void onFinish(String publicUrl, String message) {
            if (publicUrl != null) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_dev_title));
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, publicUrl);
                startActivity(Intent.createChooser(shareIntent, getString(R.string.share_dev_title)));
            }
            else {
                showToast(getString(R.string.error_share_dev_helper, message), Toast.LENGTH_LONG);
            }
        }

        @Override
        public void onScheduled() {
        }
    };

    /**
     * Main-Menu-Handler
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save:
                if (getTitle() == null || getTitle().equals("")) {
                    showToast(getString(R.string.error_invalid_title), Toast.LENGTH_LONG);
                }
                else if (getURL() == null || getURL().equals("") || !isValidUrl(getURL())) {
                    showToast(getString(R.string.error_invalid_pt_url), Toast.LENGTH_LONG);
                }
                else if (getToken() == null || getToken().equals("")) {
                    showToast(getString(R.string.error_invalid_token), Toast.LENGTH_LONG);
                }
                else if (getDevicename() == null || getDevicename().equals("")) {
                    showToast(getString(R.string.error_invalid_devname), Toast.LENGTH_LONG);
                }
                else if (getMindistance() == 0) {
                    showToast(getString(R.string.error_invalid_mindistance), Toast.LENGTH_LONG);
                }
                else if (getMintime() == 0) {
                    showToast(getString(R.string.error_invalid_mintime), Toast.LENGTH_LONG);
                }
                else if (getMinaccuracy() == 0) {
                    showToast(getString(R.string.error_invalid_minaccuracy), Toast.LENGTH_LONG);
                }
                else {
                    saveLogjob(null);
                    listener.close();
                }
                return true;
            case R.id.menu_fromLogUrl:
                fromUrlDialog.show();
                return true;
            case R.id.menu_selectSession:
                selectDialog.show();
                return true;
            case R.id.menu_share:
                String token = getToken();
                String devicename = getDevicename();
                String nextURL = getURL().replaceAll("/+$", "");
                SessionServerSyncHelper syncHelper = db.getPhonetrackServerSyncHelper();
                if (syncHelper.isConfigured(this.getActivity())) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
                    String configUrl = preferences.getString(SettingsActivity.SETTINGS_URL, SettingsActivity.DEFAULT_SETTINGS)
                            .replaceAll("/+$", "");
                    if (nextURL.equals(configUrl)) {
                        if (!syncHelper.shareDevice(token, devicename, shareCallBack)) {
                            showToast(getString(R.string.error_share_dev_network), Toast.LENGTH_LONG);
                        }
                    }
                    else {
                        Log.d(getClass().getSimpleName(), "NOT THE SAME NEXTCLOUD URL");
                        showToast(getString(R.string.error_share_dev_same_url), Toast.LENGTH_LONG);
                    }
                }
                else {
                    showToast(getString(R.string.error_share_dev_configured), Toast.LENGTH_LONG);
                }

                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        System.out.println("PHONETRACK ACT CREATEDDDDDDD");

        editToken = (EditTextPreference) this.findPreference("token");
        editToken.setText(logjob.getToken());
        editToken.setSummary(logjob.getToken());
        editDevicename = (EditTextPreference) this.findPreference("devicename");
        editDevicename.setText(logjob.getDeviceName());
        editDevicename.setSummary(logjob.getDeviceName());

        // manage session list
        sessionList = db.getSessionsNotShared();
        sessionNameList = new ArrayList<>();
        sessionIdList = new ArrayList<>();
        for (DBSession session : sessionList) {
            sessionNameList.add(session.getName());
            sessionIdList.add(String.valueOf(session.getId()));
        }

        // manage session list DIALOG
        selectBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this.getActivity(), R.style.AppThemeDialog));
        selectBuilder.setTitle(getString(R.string.edit_logjob_choose_session_dialog_title));

        if (sessionNameList.size() > 0) {
            CharSequence[] entcs = sessionNameList.toArray(new CharSequence[sessionNameList.size()]);
            selectBuilder.setSingleChoiceItems(entcs, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // user checked an item
                    setFieldsFromSession(sessionList.get(which));
                    dialog.dismiss();
                }
            });
            selectBuilder.setNegativeButton(getString(R.string.simple_cancel), null);
            selectDialog = selectBuilder.create();
        }

        // manage from URL DIALOG
        fromUrlEdit = new EditText(getContext());
        fromUrlBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this.getActivity(), R.style.AppThemeDialog));
        fromUrlBuilder.setMessage(getString(R.string.dialog_msg_import_pt_url));
        fromUrlBuilder.setTitle(getString(R.string.dialog_title_import_pt_url));

        fromUrlBuilder.setView(fromUrlEdit);

        fromUrlBuilder.setPositiveButton(getString(R.string.simple_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                setFieldsFromPhoneTrackLoggingUrl(fromUrlEdit.getText().toString());
            }
        });
        fromUrlBuilder.setNegativeButton(getString(R.string.simple_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });
        fromUrlDialog = fromUrlBuilder.create();

        // show select session dialog if there are sessions
        if (sessionNameList.size() > 0 && logjob.getTitle().equals("")) {
            selectDialog.show();
        }
    }

    private String getToken() {
        return editToken.getText();
    }
    private String getDevicename() {
        return editDevicename.getText();
    }

    private void setFieldsFromSession(DBSession s) {
        editTitle.setText(getString(R.string.logjob_title_log_to, s.getName()));
        editTitle.setSummary(getString(R.string.logjob_title_log_to, s.getName()));
        editURL.setText(s.getNextURL());
        editURL.setSummary(s.getNextURL());
        editToken.setText(s.getToken());
        editToken.setSummary(s.getToken());
    }

    private void setFieldsFromPhoneTrackLoggingUrl(String url) {
        String[] spl = url.split("/apps/phonetrack/");
        if (spl.length == 2) {
            String nextURL = spl[0];
            if (nextURL.contains("index.php")) {
                nextURL = nextURL.replace("index.php", "");
            }

            String right = spl[1];
            String[] spl2 = right.split("/");
            if (spl2.length > 2) {
                String token;
                String[] splEnd;
                // example .../apps/phonetrack/logGet/token/devname?lat=0.1...
                if (spl2.length == 3) {
                    token = spl2[1];
                    splEnd = spl2[2].split("\\?");
                }
                // example .../apps/phonetrack/log/osmand/token/devname?lat=0.1...
                else {
                    token = spl2[2];
                    splEnd = spl2[3].split("\\?");
                }
                String devname = splEnd[0];
                editTitle.setText("From PhoneTrack logging URL");
                editTitle.setSummary("From PhoneTrack logging URL");
                editDevicename.setText(devname);
                editDevicename.setSummary(devname);
                editToken.setText(token);
                editToken.setSummary(token);
                editURL.setText(nextURL);
                editURL.setSummary(nextURL);
            }
        }
    }
}
